package main

import "fmt"

func foo() {
	defer fmt.Println("Done")
	defer fmt.Println("stuff")
	fmt.Println("Doing")
}

func main() {
	foo()
}